package co.com.valkiriasoft.backlog.entity.projection;

import org.springframework.data.rest.core.config.Projection;

import co.com.valkiriasoft.backlog.entity.Backlog;
import co.com.valkiriasoft.backlog.entity.BacklogType;
import co.com.valkiriasoft.backlog.entity.Customer;

@Projection(name = "backlog", types = { Backlog.class })
public interface BacklogProjection {

	public String getId();

	public BacklogType getBacklogType();

	public Customer getCustomer();

	public String getName();

	public int getEstimatedTime();

	public String getState();

}
