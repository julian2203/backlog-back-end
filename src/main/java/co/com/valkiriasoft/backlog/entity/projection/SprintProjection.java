package co.com.valkiriasoft.backlog.entity.projection;

import java.util.List;

import org.springframework.data.rest.core.config.Projection;

import co.com.valkiriasoft.backlog.entity.Sprint;

@Projection(name = "sprintBacklog", types = { Sprint.class })
public interface SprintProjection {

	public String getId();

	public int getPriority();

	public String getName();

	public List<BacklogProjection> getBacklogs();

}
