package co.com.valkiriasoft.backlog.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import co.com.valkiriasoft.backlog.entity.Backlog;
import co.com.valkiriasoft.backlog.entity.projection.BacklogProjection;

@RepositoryRestResource(excerptProjection = BacklogProjection.class)
@CrossOrigin
public interface BacklogRepository extends JpaRepository<Backlog, String> {

}
