package co.com.valkiriasoft.backlog.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import co.com.valkiriasoft.backlog.entity.Sprint;
import co.com.valkiriasoft.backlog.entity.projection.SprintProjection;

@RepositoryRestResource(excerptProjection = SprintProjection.class)
@CrossOrigin
public interface SprintRepository extends JpaRepository<Sprint, String> {

}
