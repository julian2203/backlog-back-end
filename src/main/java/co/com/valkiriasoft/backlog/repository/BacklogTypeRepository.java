package co.com.valkiriasoft.backlog.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import co.com.valkiriasoft.backlog.entity.BacklogType;

@RepositoryRestResource
@CrossOrigin
public interface BacklogTypeRepository extends JpaRepository<BacklogType, String> {

}
