package co.com.valkiriasoft.backlog.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import co.com.valkiriasoft.backlog.entity.Customer;

@RepositoryRestResource
@CrossOrigin
public interface CustomerRepository extends JpaRepository<Customer, String> {

}
